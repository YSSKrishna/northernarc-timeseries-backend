from flask import Flask, render_template, request, jsonify
from flask_restful import Resource, Api
import pandas as pd
from pmdarima.arima import auto_arima
from math import sqrt
from sklearn.metrics import mean_squared_error
from flask_cors import CORS
import psycopg2
from datetime import date
from calendar import monthrange

app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})

api = Api(app)

@app.route("/")
def index():
    return render_template('index.html')

def getQuarterEnd(row):
    if row['quarter'].astype(int) in [1,2,3]:
        return date(row['financial_year'].astype(int), 3*row['quarter'].astype(int) +3 , monthrange(row['financial_year'].astype(int), 3*row['quarter'].astype(int)+3)[1]).strftime("%Y-%m-%d")
    else:
        return date(row['financial_year'].astype(int)+1, 3, monthrange(row['financial_year'].astype(int) +1, 3)[1]).strftime("%Y-%m-%d")

class Predict(Resource):
    def post(self):
        request_json = request.get_json()
        print(request_json)

        data = pd.read_csv(r'F:\Yenugu\northernarc-timeseries-backend\datasets\dataset.csv')
        # data.Date = pd.to_datetime(data.Date)
        dates = pd.date_range(data.iloc[-1, 0], periods=request_json['ahead']+1, freq='Q', closed='right').astype(str)

        train = data.copy()
        train.drop('Date', axis=1, inplace=True)

        # building the model
        model = auto_arima(train, trace=True, error_action='ignore', suppress_warnings=True)
        model.fit(train)

        forecast = model.predict(n_periods=request_json['ahead'])
        forecast = pd.DataFrame({'Date': pd.Series(dates), 'Prediction': pd.Series(forecast)})

        return {'data':{
            'existing':data.to_dict('records'),
            'forecast':forecast.to_dict('records')
        }}, 200

class PredictError(Resource):
    def post(self):
        request_json = request.get_json()
        print(request_json)

        table_name ='balance_sheet'
        conn = psycopg2.connect(host="xxxxxxx",database="xxxxxx", user="xxxxx", password="xxxxxxx", port="xxxx")
        test_query  = "SELECT financial_year, quarter, balance_sheet-> 'capital' -> 'networth' AS networth from finance."+table_name +" WHERE entity_id = '5158836861539344' AND financial_year > 2013 ORDER BY financial_year, quarter ASC"

        df = pd.read_sql(test_query, con=conn);
        df['Date'] = df.apply(getQuarterEnd,axis = 1)
        print(df)

        data = df[['Date','networth']]
        data.rename(columns= {'Date':'Date', 'networth':'Amount'}, inplace=True)
        print(data)

        dates = pd.date_range(data.iloc[-1-request_json['validate'], 0], periods=request_json['ahead'] + 1, freq='Q', closed='right').astype(str)
        print(dates)

        train = data[:(len(data)- request_json['validate'])].copy()
        valid = data[(len(data)- request_json['validate']):].copy()
        train.drop('Date', axis=1, inplace=True)
        valid.drop('Date', axis=1, inplace=True)
        print(train)
        print(valid)

        # building the model
        model = auto_arima(train, trace=True, error_action='ignore', suppress_warnings=True, )
        model.fit(train)

        forecast, conf_int = model.predict(n_periods=request_json['ahead'], return_conf_int=True)
        conf_int = pd.DataFrame(conf_int, columns=["LB","UB"])

        conf_int = pd.concat([pd.DataFrame({'Date': pd.Series(dates)}),conf_int], axis=1)
        print(forecast)
        print(conf_int)

        print(request_json['validate'])
        if request_json['validate'] !=0:
            rms = sqrt(mean_squared_error(valid, forecast))
            print(rms)

        forecast = pd.DataFrame({'Date': pd.Series(dates), 'Prediction': pd.Series(forecast)})
        if request_json['validate'] != 0:
            return {'data': {
                'existing': data.to_dict('records'),
                'forecast': forecast.to_dict('records'),
                'conf_int': conf_int.to_dict('records'),
                'rms': rms
            }}, 200
        else:
            return {'data': {
                'existing': data.to_dict('records'),
                'forecast': forecast.to_dict('records'),
                'conf_int': conf_int.to_dict('records')
            }}, 200

api.add_resource(Predict, '/predict')
api.add_resource(PredictError, '/predict-error')

#run command
#py -3 app.py

if __name__ == '__main__':
    app.run(debug=True)